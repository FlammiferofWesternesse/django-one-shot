from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoItem, TodoList
from .forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method != "POST":
        return render(request, "todos/delete.html")
    else:
        todo.delete()
        return redirect("todo_list_list")


def todo_item_create(request):
    if request.method != "POST":
        form = TodoItemForm()
    else:
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    context = {
        "form": form,
    }
    return render(request, "todos/create-item.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method != "POST":
        form = TodoItemForm(instance=item)
    else:
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    context = {
        "form": form,
    }
    return render(request, "todos/update-item.html", context)


def todo_list_create(request):
    if request.method != "POST":
        form = TodoListForm()
    else:
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)

    context = {"form": form}
    return render(request, "todos/create-list.html", context)


def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/todo_lists.html", context)


def todo_list_update(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method != "POST":
        form = TodoListForm(instance=todo)
    else:
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    context = {
        "form": form,
    }
    return render(request, "todos/update-list.html", context)
